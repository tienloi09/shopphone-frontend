import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import PagePhone from '../views/PagePhone.vue'
import PagePhoneDetail from '../views/PagePhoneDetail.vue'
import PageLaptop from '../views/PageLaptop.vue'
import PageLaptopDetail from '../views/PageLaptopDetail.vue'
import PageError from '../views/PageError.vue'
import Cart from '../views/Cart.vue'
import Pay from '../views/Pay.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/cart',
    name: 'Cart',
    component: Cart
  },
  {
    path: '/pay',
    name: 'Pay',
    component: Pay
  },
  {
    path: '/dien-thoai',
    name: 'PagePhone',
    component: PagePhone
  },
  {
    path: '/dien-thoai/:id',
    name: 'PagePhoneDetail',
    component: PagePhoneDetail
  },
  {
    path: '/laptop',
    name: 'PageLaptop',
    component: PageLaptop
  },
  {
    path: '/laptop/:id',
    name: 'PageLaptopDetail',
    component: PageLaptopDetail
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'PageError',
    component: PageError
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
