import { createStore } from 'vuex'

const state = {
    cart: [],
};
const mutations = {
    ADD_CART(state,item){ 
        state.cart.push(item);
    },
    ADD(state,item){
        state.cart.push(item);
    }
};
const actions ={
    addTodo({ commit } , item){
        commit('ADD',item);
    }
}

export default createStore({
    state,mutations,actions
})
